/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'

eventsApp.directive('repeatX', function () {
    return {
        // compile function runs once and affects all instances of the directive
        //  we use compile for performance
        compile: function (element, attributes) {
            for (var i = 0; i < Number(attributes.repeatX) - 1; i++) {
                // set 0 for cloned elements to avoid infinite loop
                element.after(element.clone().attr('repeat-x', 0));
            }
            // link function
            return function (scope, element, attributes, controller) {
                attributes.$observe('text', function (newValue) {
                    if (newValue === 'Hello World') {
                        element.css('color', 'red');
                    } else {
                        element.css('color', 'blue');
                    }
                });
            }
        }
        // link function runs individually for each directive
        //link: function (scope, element, attributes, controller) {
        //    for (var i = 0; i < Number(attributes.repeatX) - 1; i++) {
        //        // set 0 for cloned elements to avoid infinite loop
        //        element.after($compile(element.clone().attr('repeat-x', 0))(scope));
        //    }
        //}
    }
});