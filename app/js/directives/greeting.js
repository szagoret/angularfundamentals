/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'

eventsApp.directive('greeting', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: "<div><button class='btn' ng-click='sayHello()'>Say Hello</button><div ng-transclude></div></div>",
        controller: function ($scope) {
            var greetings = ['hello'];

            $scope.sayHello = function () {
                alert(greetings.join());
            };

            $scope.addGreeting = function (greeting) {
                greetings.push(greeting);
            }
        }
        //controller: 'GreetingController'
        //controller: '@',
        //name: 'ctrl'
    };
})
    .directive('finnish', function () {
        return {
            restrict: 'A',
            require: '^greeting', // ^ -> look upwards until find a greeting directive
            link: function (scope, element, attrs, controller) {
                //controller.addGreeting('hei');
                scope.addGreeting('hei');
            }
        }
    })
    .directive('hindi', function () {
        return {
            restrict: 'A',
            require: '^greeting',
            link: function (scope, element, attrs, controller) {
                //controller.addGreeting('??????');
                scope.addGreeting('??????');
            }
        }
    });

//eventsApp.controller('GreetingController', function GreetingController($scope) {
//    $scope.sayHello = function () {
//        alert('Hello');
//    }
//});