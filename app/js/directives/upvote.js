/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'


eventsApp.directive('upvote', function ($compile) {
    return {
        restrict: 'E',
        templateUrl: '/templates/directives/upvote.html',
        scope: {
            // & -> provide to execute a function to a parent scope
            upvote: "&",
            downvote: "&",
            // @ -> attend a angular expression
            // = -> attend a concrete value
            count: "="
        }
    }
});