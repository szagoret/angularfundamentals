/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'

eventsApp.directive('mySample', function ($compile) {
    return {
        restrict: 'E',
        template: "<input type='text' ng-model='sampleData' />{{sampleData}}<br />",
        scope: {

        }
        //link: function (scope, element, attrs, controller) {
        //    var markup = "<input type='text' ng-model='sampleData' />{{sampleData}}<br />";
        //    //this $scope is for SampleDirective page
        //    angular.element(element).html($compile(markup)(scope));
        //}
    }
});