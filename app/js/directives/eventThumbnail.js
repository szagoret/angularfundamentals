/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'


eventsApp.directive('eventThumbnail', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/templates/directives/eventThumbnail.html',
        scope: {
            event: "="
        }
    }
});