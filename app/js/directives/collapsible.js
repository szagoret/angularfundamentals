/**
 * Created by szagoret on 4/23/2016.
 */
'use strict'

eventsApp.directive('collapsible', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><h4 class="panel-title" ng-click="toggleVisibility()">{{title}}</h4><div class="sample-show-hide" ng-show="visible" ng-transclude></div></div>',
        transclude: true,
        controller: function ($scope) {
            $scope.visible = true;
            $scope.toggleVisibility = function () {
                $scope.visible = !$scope.visible;
            }
        },
        scope: {
            title: '@'
        }
    }
});