/**
 * Created by szagoret on 3/21/2016.
 */
eventsApp.factory('eventData', function ($resource) {
    var resource = $resource('/data/event/:id', {id: '@id'});

    return {
        getEvent: function (eventId) {
            return resource.get({id: eventId});
        },
        save: function (event) {
            event.id = 99;
            return resource.save(event);
        },
        getAllEvents:function(){
            return resource.query();
        }
    };
});