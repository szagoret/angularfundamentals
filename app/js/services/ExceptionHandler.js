/**
 * Created by szagoret on 4/9/2016.
 */
'use strict'
// override default service
eventsApp.factory('$exceptionHandler', function () {
    return function (exception) {
        console.log("exception handler: " + exception.message);
    }
});