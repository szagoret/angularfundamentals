'user strict'

var eventsApp = angular.module('eventsApp', ['ngResource', 'ngRoute'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/newEvent', {
            templateUrl: 'templates/NewEvent.html',
            controller: 'EditEventController'
        });
        $routeProvider.when('/events', {
            templateUrl: 'templates/EventList.html',
            controller: 'EventListController'
        });
        $routeProvider.when('/events/:eventId', {
            foo: 'bar',
            templateUrl: 'templates/EventDetails.html',
            controller: 'EventController',
            resolve: { //delay display page for obtain parameters
                event: function ($route, eventData) {
                    return eventData.getEvent($route.current.pathParams.eventId).$promise;
                }
            }
        });

        $routeProvider.when('/sampleDirective', {
            templateUrl: 'templates/SampleDirective.html',
            controller: 'SampleDirectiveController'
        });

        $routeProvider.otherwise({redirectTo: '/events'});

        $locationProvider.html5Mode(true);
    });
var contactManager = angular.module('contactManager', ['ngMaterial']);