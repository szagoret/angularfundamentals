/**
 * Created by szagoret on 3/4/2016.
 */
'use strict'

eventsApp.controller('EventController',
    function EventController($scope, eventData, $log, $anchorScroll, $routeParams, $route) {

        console.log($route.current.foo);
        console.log($route.current.params.foo);
        console.log($route.current.pathParams.eventId);
        $scope.reload = function () {
            $route.reload();
        };
        $scope.event = $route.current.locals.event;
        eventData.getEvent($routeParams.eventId).$promise.then(
            function (event) {
                $scope.event = event;
                $log.info(event)
            },
            function (response) {
                $log.error(response)
            }
        );

        $scope.upVoteSession = function (session) {
            session.upVoteCount++;
        };
        $scope.downVoteSession = function (session) {
            session.upVoteCount--;
        };
        $scope.scrollToSession = function () {
            $anchorScroll();
        };
    });

