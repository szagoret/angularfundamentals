/**
 * Created by szagoret on 4/9/2016.
 */
'use strict'

eventsApp.controller('CookieStoreSampleController', function CookieStoreSampleController($scope, $cookieStore) {
    $scope.event = {id: 1, name: "My Event"};

    $scope.saveEventToCookie = function (event) {
        $cookieStore.put('event', event);
    };

    $scope.getEventFroCookie = function () {
        console.log($cookieStore.get('event'));
    };

    $scope.removeEventFromCookie = function () {
        $cookieStore.remove('event');
    };
});