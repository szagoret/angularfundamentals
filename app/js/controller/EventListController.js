/**
 * Created by szagoret on 4/12/2016.
 */
'use strict'

eventsApp.controller('EventListController', function ($scope, eventData) {
    $scope.events = eventData.getAllEvents();
});