/**
 * Created by szagoret on 4/14/2016.
 */
'use strict'
eventsApp.controller('NavigationController', function ($scope, $location) {

    console.log('absUrl', $location.absUrl());
    console.log('protocol', $location.protocol());
    console.log('port', $location.port());
    console.log('host', $location.host());
    console.log('path', $location.path());
    console.log('search', $location.search());
    console.log('hash', $location.hash());
    console.log('url', $location.url());

    $scope.isActive = function (viewLocation) {
        var active = (viewLocation === $location.path());
        return active;
    };

    $scope.createEvent = function () {
        $location.replace();  // replace current page with another avoid history in the browser
        $location.url('/newEvent');
    };
});