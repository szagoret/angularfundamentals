'use strict'

eventsApp.controller("EditEventController", function ($scope, eventData, $log) {
    $scope.event = {};
    $scope.saveEvent = function (event, newEventForm) {
        if (newEventForm.$valid) {
            eventData.save(event)
                .$promise
                .then(function (response) {$log.info('success', response)}
                .catch(function (response) {$log.error('failure', response)})
            );
        }
    };
    $scope.cancelEdit = function (event) {
        window.location = "/EventDetails.html";
    }
});