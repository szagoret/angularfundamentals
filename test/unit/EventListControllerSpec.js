/**
 * Created by szagoret on 5/3/2016.
 */
'use strict';

describe('EventListController', function () {
    var $controllerConstructor, scope, mockEventData;
    /**
     * Because controllers are not available on the global scope,
     * we need to use angular.mock.inject to inject our controller first.
     */

    /**
     * module function is provided by angular-mocks
     */
    beforeEach(module("eventsApp"));
    //$controller -> the service that is responsible for instantiating controllers
    beforeEach(inject(function ($controller, $rootScope) {
        $controllerConstructor = $controller;
        scope = $rootScope.$new();
        mockEventData = sinon.stub({
            getAllEvents: function () {
            }
        });
    }));

    it('should set the scope events to the result of events eventData.getAllEvents', function () {
        var mockEvents = {};
        mockEventData.getAllEvents.returns(mockEvents);
        $controllerConstructor("EventListController",
            {'$scope': scope, eventData: mockEventData}
        );
        expect(scope.events).toBe(mockEvents);
    });
});